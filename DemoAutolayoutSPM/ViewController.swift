//
//  ViewController.swift
//  DemoAutolayoutSPM
//
//  Created by Ly Boung on 7/9/23.
//

import UIKit
import AutoLayoutSPM

class ViewController: UIViewController {

  lazy var backgroundView = UIView().customize {
    $0.backgroundColor = .lightGray.withAlphaComponent(0.3)
  }
  lazy var label = UILabel().customize {
    $0.text = "Welcome to Autolayout SPM"
    $0.textAlignment = .center
    $0.textColor = .black
    $0.font = .boldSystemFont(ofSize: 15)
  }
  lazy var centerXButton = UIButton().customize {
    $0.setTitle("Center X Button", for: .normal)
    $0.setTitleColor(.white, for: .normal)
    $0.backgroundColor = .systemBlue
  }
  lazy var centerButton = UIButton().customize {
    $0.setTitle("Center Button", for: .normal)
    $0.setTitleColor(.white, for: .normal)
    $0.backgroundColor = .systemBlue
  }
  lazy var bottomButton = UIButton().customize {
    $0.setTitle("Buttom Button", for: .normal)
    $0.setTitleColor(.white, for: .normal)
    $0.backgroundColor = .systemBlue
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareLayouts()
  }

  private func prepareLayouts() {
    view.backgroundColor = .white
    
    backgroundView.layoutView {
      view.addSubview($0)
      $0.fill(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }
    
    label.layoutView {
      backgroundView.addSubview($0)
      $0.top(constraint: backgroundView.topAnchor, constant: 16, priority: .equal)
      $0.leading(constraint: view.leadingAnchor, constant: 16)
      $0.trailing(constraint: view.trailingAnchor, constant: 16)
    }
    
    centerXButton.layoutView {
      backgroundView.addSubview($0)
      $0.centerX()
        .top(constraint: label.bottomAnchor, constant: 16)
        .height(constant: 50)
        .width(constant: 200)
    }
    
//  Can use center() or can use centerX() and centerY() as comment below
    centerButton.layoutView {
      backgroundView.addSubview($0)
      $0.center()
      $0.height(constant: 200)
      $0.width(constant: 200)
    }
    
//    centerButton.layoutView {
//      backgroundView.addSubview($0)
//      $0.centerX()
//      $0.centerY()
//      $0.height(constant: 50)
//      $0.width(constant: 200)
//    }
    
    bottomButton.layoutView {
      backgroundView.addSubview($0)
      $0.centerX()
      $0.leading(constant: 16)
      $0.bottom(constant: 16)
      $0.height(constant: 50)
    }
    
    // MARK: - Rendering Function
    centerXButton.setCornerRadiusOnly(corners: [.topLeft, .topRight], constant: 16)
    centerButton.roundView()
    bottomButton.setBorder(width: 1, color: .red)
  }
}

