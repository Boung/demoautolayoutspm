//
//  AppDelegate.swift
//  DemoAutolayoutSPM
//
//  Created by Ly Boung on 7/9/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    configRootViewController()
    return true
  }
  
  private func configRootViewController() {
    window = UIWindow()
    window?.rootViewController = ViewController()
    window?.makeKeyAndVisible()
  }
}
